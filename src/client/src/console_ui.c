#include "console_ui.h"

#include <chess_lib/chess.h>

#include <console_utils/console_tools.h>
#include <console_utils/input_tools.h>

#include <stdlib.h>
#include <string.h>

#if defined(__linux__)
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>

#include <unistd.h>
#else
#error "Only Linux supported!"
#endif

void  DrawColoredGameField(GameState* pGameState, Color player) {
	if (pGameState == NULL)
		return;

	ClearConsole();
	int row;
	for ((player == WHITE) ? (row = 7) : (row = 0); (player == WHITE) ? (row >= 0) : (row < 8); (player == WHITE) ? (--row) : (++row)){
		printf("(%d)", row + 1);
		SetConsoleStyle(BRIGHT);
		int col;
		for ((player == WHITE) ? (col = 0) : (col = 7); (player == WHITE) ? (col < 8) : (col >= 0); (player == WHITE) ? (++col) : (--col)){
			switch ((row + col) % 2) {
			case 0: SetBackgroundColor(126,  98,  86); break;
			case 1: SetBackgroundColor(229, 208, 163); break;
			}
			if (pGameState->gamefield[col + 8 * row] == NULL)
				printf("   ");
			else {
				switch (pGameState->gamefield[col + row * 8]->color){
				case BLACK: SetFontColor(  0,   0,   0); break;
				case WHITE: SetFontColor(255, 255, 255); break;
				case NONE : break;
				}
				switch (pGameState->gamefield[col + row * 8]->type){
				case ROOK	: printf("%s", " R "); break;
				case BISHOP	: printf("%s", " B "); break;
				case KNIGHT	: printf("%s", " N "); break;
				case QUEEN	: printf("%s", " Q "); break;
				case KING	: printf("%s", " K "); break;
				case PAWN	: printf("%s", " P "); break;
				default     : printf("%s", "ERR");
				}
			}
		}
		ResetConsoleColors();
		printf("\n");
	}
	printf("%s", "   ");
	int i;
	for ((player == WHITE) ? (i = 0) : (i = 7); (player == WHITE) ? (i < 8) : (i >= 0); (player == WHITE) ? (++i) : (--i))
		printf("(%c)", 'a' + i);
	printf("%s", "\n");
	ResetConsoleColors();
}

void DrawGameField(GameState* pGameState, Color player) {
	if (pGameState == NULL)
			return;

	int row = 0;
	for ((player == WHITE) ? (row = 7) : (row = 0); (player == WHITE) ? (row >= 0) : (row < 8); (player == WHITE) ? (--row) : (++row)){
		printf("(%d)", row + 1);
		int col;
		for ((player == WHITE) ? (col = 0) : (col = 7); (player == WHITE) ? (col < 8) : (col >= 0); (player == WHITE) ? (++col) : (--col)){
			if (pGameState->gamefield[col + 8 * row] == NULL)
				printf("   ");
			else {
				switch (pGameState->gamefield[col + row * 8]->color){
				case BLACK: printf("%s", "b"); break;
				case WHITE: printf("%s", "w"); break;
				case NONE : printf("%s", "e"); break;
				}
				switch (pGameState->gamefield[col + row * 8]->type){
				case ROOK	: printf("%s", "R "); break;
				case BISHOP	: printf("%s", "B "); break;
				case KNIGHT	: printf("%s", "N "); break;
				case QUEEN	: printf("%s", "Q "); break;
				case KING	: printf("%s", "K "); break;
				case PAWN	: printf("%s", "P "); break;
				default     : printf("%s", "ERR"); break;
				}
			}
		}
		printf("\n");
	}
	printf("%s", "   ");
	int i;
	for ((player == WHITE) ? (i = 0) : (i = 7); (player == WHITE) ? (i < 8) : (i >= 0); (player == WHITE) ? (++i) : (--i))
		printf("(%c)", 'a' + i);
	printf("%s", "\n");
}

void PrintHelp(){
	printf("%s", "==HELP==\n");
	printf("%s", "\"xayb\"  - move piece from xa to yb\n");
	printf("%s", "\"^F\"    - promote pawn to piece F (R, N, B or Q)\n");
	printf("%s", "\"0-0-0\" - perform castling (queen side)\n");
	printf("%s", "\"0-0\"   - perform castling (king side)\n");
	printf("%s", "\"(=)\"   - propose draw\n");
	printf("%s", "\"(y)\"   - accept draw\n");
	printf("%s", "\"(n)\"   - reject draw\n");
	printf("%s", "\"(F)\"   - forfeit\n");
}

ChessMove GetPlayerMove(Color player) {
	us_int input_ok = 0;
	printf("[%s move]: ", (player == WHITE) ? "White" : "Black");
	char* input = readline();
	ChessMove ans = {
		.pMove = NULL,
		.pSpecialMove = NULL
	};
	while (!input_ok){
		if (strcmp(input, "help") == 0) {
			PrintHelp();
			free(input);
			printf("%s", "\n");
			printf("[%s move]: ", (player == WHITE) ? "White" : "Black");
			input = readline();
		}
		else if (strcmp(input, "0-0-0") == 0) {
			ans.pSpecialMove = (SpecialMove*)malloc(sizeof(SpecialMove));
			ans.pSpecialMove->player = player;
			ans.pSpecialMove->moveType = CASTLING_QUEENSIDE;
		} else if (strcmp(input, "0-0") == 0) {
			ans.pSpecialMove = (SpecialMove*)malloc(sizeof(SpecialMove));
			ans.pSpecialMove->player = player;
			ans.pSpecialMove->moveType = CASTLING_KINGSIDE;
		} else if (strcmp(input, "(=)") == 0) {
			ans.pSpecialMove = (SpecialMove*)malloc(sizeof(SpecialMove));
			ans.pSpecialMove->player = player;
			ans.pSpecialMove->moveType = DRAW_PROPOSAL;
		} else if (strcmp(input, "(y)") == 0) {
			ans.pSpecialMove = (SpecialMove*)malloc(sizeof(SpecialMove));
			ans.pSpecialMove->player = player;
			ans.pSpecialMove->moveType = DRAW_ACCEPT;
		} else if (strcmp(input, "(n)") == 0) {
			ans.pSpecialMove = (SpecialMove*)malloc(sizeof(SpecialMove));
			ans.pSpecialMove->player = player;
			ans.pSpecialMove->moveType = DRAW_REJECT;
		} else if (strcmp(input, "(F)") == 0) {
			ans.pSpecialMove = (SpecialMove*)malloc(sizeof(SpecialMove));
			ans.pSpecialMove->player = player;
			ans.pSpecialMove->moveType = FORFEIT;
		} else if (input[0] == '^'){
			if (strlen(input) != 2) {
				printf("Error: Promotion command length mismatch! Should be 2, got %lu\n", strlen(input));
				free(input);
				printf("%s", "Please, repeat input\n");
				printf("[%s move]: ", (player == WHITE) ? "White" : "Black");
				input = readline();
				continue;
			}
			ans.pSpecialMove = (SpecialMove*)malloc(sizeof(SpecialMove));
			ans.pSpecialMove->player = player;
			ans.pSpecialMove->moveType = PROMOTION;
			switch (input[1]){
			case 'R': ans.pSpecialMove->promoteTo = ROOK  ; break;
			case 'N': ans.pSpecialMove->promoteTo = KNIGHT; break;
			case 'B': ans.pSpecialMove->promoteTo = BISHOP; break;
			case 'Q': ans.pSpecialMove->promoteTo = QUEEN ; break;
			default :
				printf("Error: Wrong promotion target! Should be R, N, B or Q, got \'%c\'.\n", input[1]);
				free(ans.pSpecialMove);
				free(input);
				printf("%s", "Please, repeat input\n");
				printf("[%s move]: ", (player == WHITE) ? "White" : "Black");
				input = readline();
				continue;
				break;
			}
		} else {
			if (strlen(input) != 4) {
				printf("Error: Regular move command length mismatch! Should be 4, got %lu.\n", strlen(input));
				free(input);
				printf("%s", "Please, repeat input\n");
				printf("[%s move]: ", (player == WHITE) ? "White" : "Black");
				input = readline();
				continue;
			}
			ans.pMove = (Move*)malloc(sizeof(Move));
			ans.pMove->startColumn 	= input[0] - 'a';
			ans.pMove->startRow 	= input[1] - '1';
			ans.pMove->finishColumn = input[2] - 'a';
			ans.pMove->finishRow	= input[3] - '1';
			ans.pMove->player 		= player;
			ans.pMove->pMoveSup 	= NULL;
		}
		input_ok = 1;
	}
	free(input);
	return ans;
}

us_int RequestYesOrNo(const char* question) {
	if (question == NULL)
		return 0;

	printf("%s[yes/no]:\n", question);
	us_int input_ok = 0;
	us_int ans = 0;
	while (!input_ok) {
		char* input = readline();
		if (strcmp(input, "yes") == 0){
			ans = 1;
			input_ok = 1;
		} else if (strcmp(input, "no") == 0){
			ans = 0;
			input_ok = 1;
		}
		free(input);
		if (!input_ok) {
			printf("Please enter \"yes\" or \"no\"\n");
			printf("%s[yes/no]:\n", question);
		}
	}
	return ans;
}

void ProcessMove(GameState* pGameState) {
	if (pGameState == NULL)
		return;
	us_int move_ok = 0;
	ChessMove playerMove = {
		.pMove = NULL,
		.pSpecialMove = NULL
	};
	while (!move_ok) {
		playerMove = GetPlayerMove(pGameState->currentPlayer);
		move_ok = MakeMove(pGameState, &playerMove);
		if (!move_ok){
			if (playerMove.pMove != NULL) {
				free(playerMove.pMove);
				playerMove.pMove = NULL;
			}
			else if (playerMove.pSpecialMove) {
				free(playerMove.pSpecialMove);
				playerMove.pSpecialMove = NULL;
			}
			printf("Error: Your move is incorrect. Please, try again.\n");
		}
	}
	if (playerMove.pMove != NULL)
		free(playerMove.pMove);
	if (playerMove.pSpecialMove != NULL)
		free(playerMove.pSpecialMove);
}

void PlayChess() {
	ChessPiece** chessPieces = StandartChessPreset();
	GameState* pGameState = GameState_init(chessPieces, 32);
	us_int is_colored = RequestYesOrNo("Would you like to use colored version?");
	Color winner = NONE;
	while (!IsFinished(pGameState, &winner)) {
		if (is_colored)
			DrawColoredGameField(pGameState, pGameState->currentPlayer);
		else
			DrawGameField(pGameState, pGameState->currentPlayer);
		if (pGameState->draw_proposed)
			printf("%s proposes draw\n", (pGameState->currentPlayer == BLACK) ? "White" : "black");
		ProcessMove(pGameState);
	}
	if (is_colored)
		DrawColoredGameField(pGameState, pGameState->currentPlayer);
	else
		DrawGameField(pGameState, pGameState->currentPlayer);

	switch (winner){
	case WHITE: printf("%s is the winner!", "White"); break;
	case BLACK: printf("%s is the winner!", "Black"); break;
	case NONE:  printf("Match ended in %s!", "draw"); break;
	}

	ListIter* pListIter = GetMovesLog(pGameState);
	char* pCurrent = ListIter_getnext(pListIter);
	printf("\n\nLOG:\n");
	us_int counter = 1;
	while (pCurrent != NULL) {
		printf("%d. %s ", counter, pCurrent);
		pCurrent = ListIter_getnext(pListIter);
		if (pCurrent != NULL) {
			printf("%s\n", pCurrent);
			pCurrent = ListIter_getnext(pListIter);
		}
		++counter;
	}
	printf("\n");
	ListIter_fini(&pListIter);

	GameState_fini(&pGameState);
}

struct in_addr GetServerAddress() {
	struct in_addr ans;
	printf("Please enter server address: ");
	char* input = readline();
	while (inet_aton(input, &ans) == 0){
		free(input);
		printf("Error: Server address is incorrect.\nPlease, try again: ");
		input = readline();
	}
	return ans;
}

int ConnectToServer() {
	int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (socket_fd == -1) {
		printf("Fatal error: Socket creation failed.\n");
		exit(0);
	}
	struct sockaddr_in serv_addr;
	memset(&serv_addr, 0, sizeof(struct sockaddr_in));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(11374);
	serv_addr.sin_addr = GetServerAddress();
	while (connect(socket_fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) != 0) {
		printf("Error: Connection to server failed.\n");
		us_int ans = RequestYesOrNo("Do you wish to try again?");
		if (ans)
			serv_addr.sin_addr = GetServerAddress();
		else
			exit(0);
	}

	return socket_fd;
}

Color ServerHandshake(int socket_fd){
	char buf[10];
	memset(buf, 0, sizeof(buf));
	recv(socket_fd, buf, sizeof(buf), 0);
	if (buf[0] != 0x0A) {
		printf("Error: Incorrect response from server. Aborting connection.\n");
		close(socket_fd);
		exit(0);
	}
	char ans[10];
	memset(ans, 0, sizeof(ans));
	ans[0] = 0x0A;
	send(socket_fd, buf, sizeof(char) * 10, 0);
	return (Color)buf[1];
}

us_int WaitForServer(GameState* pGameState, int server_socket, Color player) {
	if (pGameState == NULL)
		return 0;
	if (server_socket < 0)
		return 0;

	char buff[10];
	recv(server_socket, buff, sizeof(char) * 10, 0);
	if (buff[0] == 0x0E) {
		printf("Error: Server error. Aborting connection.\n");
		close(server_socket);
		return 0;
	}
	if (buff[0] == 0x0B) {
		printf("GAME OVER. Aborting connection.\n");
		close(server_socket);
		return 0;
	}
	if (buff[0] != 0x0C) {
		printf("Error: Incorrect response from server. Aborting connection.\n");
		close(server_socket);
		return 0;
	}
	if (buff[1] == 0) {
		if (player == pGameState->currentPlayer)
			return 1;
		else {
			printf("Error: Incorrect response from server. Aborting connection.\n");
			close(server_socket);
			return 0;
		}
	}
	ChessMove move = {
		.pMove = NULL,
		.pSpecialMove = NULL
	};
	switch (buff[1]){
	case 1:
		move.pMove = (Move*)malloc(sizeof(Move));
		move.pMove->startColumn  = ((us_int)buff[2] << 8) | (us_int)buff[3];
		move.pMove->startRow     = ((us_int)buff[4] << 8) | (us_int)buff[5];
		move.pMove->finishColumn = ((us_int)buff[6] << 8) | (us_int)buff[7];
		move.pMove->finishRow    = ((us_int)buff[8] << 8) | (us_int)buff[9];
		move.pMove->player 		 = Inverse(player);
		move.pMove->pMoveSup 	 = NULL;
		break;
	case 2:
		move.pSpecialMove = (SpecialMove*)malloc(sizeof(SpecialMove));
		move.pSpecialMove->player = Inverse(player);
		switch (buff[2]) {
		case 0://PROMOTION
			move.pSpecialMove->moveType = PROMOTION;
			move.pSpecialMove->promoteTo = (Type)buff[3];
			break;
		case 1://CASTLING_QUEENSIDE
			move.pSpecialMove->moveType = CASTLING_QUEENSIDE;
			break;
		case 2://CASTLING_KINGSIDE
			move.pSpecialMove->moveType = CASTLING_KINGSIDE;
			break;
		case 3://DRAW_PROPOSAL
			move.pSpecialMove->moveType = DRAW_PROPOSAL;
			break;
		case 4://DRAW_ACCEPT
			move.pSpecialMove->moveType = DRAW_ACCEPT;
			break;
		case 5://DRAW_REJECT
			move.pSpecialMove->moveType = DRAW_REJECT;
			break;
		case 6://FORFEIT
			move.pSpecialMove->moveType = FORFEIT;
			break;
		default: {
			printf("Error: Incorrect response from server. Aborting connection.\n");
			close(server_socket);
			return 0;
		}
		}
		break;
	default: {
		printf("Error: Incorrect response from server. Aborting connection.\n");
		close(server_socket);
		return 0;
	}
	}
	us_int ans = MakeMove(pGameState, &move);
	if (!ans){
		printf("Error: Incorrect response from server. Aborting connection.\n");
		close(server_socket);
		return 0;
	}
	if (move.pSpecialMove != NULL)
		free(move.pSpecialMove);
	if (move.pMove != NULL)
		free(move.pMove);

	if (pGameState->playerStates[Inverse(player)].pPromotion != NULL) {
		recv(server_socket, buff, sizeof(char) * 10, 0);
		if (buff[0] == 0x0E){
			printf("Error: Server error. Aborting connection.\n");
			close(server_socket);
			return 0;
		}
		if ((buff[0] != 0x0C) || (buff[1] != 2) || (buff[2] != 0)){
			printf("Error: Incorrect response from server. Aborting connection.\n");
			close(server_socket);
			return 0;
		}
		move.pMove = NULL;
		move.pSpecialMove = (SpecialMove*)malloc(sizeof(SpecialMove));
		move.pSpecialMove->moveType = PROMOTION;
		move.pSpecialMove->promoteTo = (Type)buff[3];
		move.pSpecialMove->player = Inverse(player);
		us_int ans = MakeMove(pGameState, &move);
		if (!ans){
			printf("Error: Incorrect response from server. Aborting connection.\n");
			close(server_socket);
			return 0;
		}
		free(move.pSpecialMove);
	}
	return 1;
}

us_int SendMoveToServer(GameState* pGameState, int server_socket) {
	if (pGameState == NULL)
		return 0;
	if (server_socket < 0)
		return 0;

	us_int move_ok = 0;
	ChessMove playerMove = {
		.pMove = NULL,
		.pSpecialMove = NULL
	};
	while (!move_ok) {
		playerMove = GetPlayerMove(pGameState->currentPlayer);
		move_ok = MakeMove(pGameState, &playerMove);
		if (!move_ok){
			if (playerMove.pMove != NULL)
				free(playerMove.pMove);
			else if (playerMove.pSpecialMove)
				free(playerMove.pSpecialMove);
			printf("Error: Your move is incorrect. Please, try again.\n");
		}
	}
	char buff[10];
	buff[0] = 0x0C;
	if (playerMove.pMove != NULL) {
		buff[1] = 1;
		buff[2] = (char)((playerMove.pMove->startColumn & 0xFF00) >> 8);
		buff[3] = (char)( playerMove.pMove->startColumn & 0x00FF);
		buff[4] = (char)((playerMove.pMove->startRow & 0xFF00) >> 8);
		buff[5] = (char)( playerMove.pMove->startRow & 0x00FF);
		buff[6] = (char)((playerMove.pMove->finishColumn & 0xFF00) >> 8);
		buff[7] = (char)( playerMove.pMove->finishColumn & 0x00FF);
		buff[8] = (char)((playerMove.pMove->finishRow & 0xFF00) >> 8);
		buff[9] = (char)( playerMove.pMove->finishRow & 0x00FF);
		free(playerMove.pMove);
		playerMove.pMove = NULL;
	} else if (playerMove.pSpecialMove != NULL) {
		buff[1] = 2;
		switch (playerMove.pSpecialMove->moveType) {
		case PROMOTION://0
			buff[2] = 0;
			buff[3] = (char)playerMove.pSpecialMove->promoteTo;
			break;
		case CASTLING_QUEENSIDE://1
			buff[2] = 1;
			break;
		case CASTLING_KINGSIDE://2
			buff[2] = 2;
			break;
		case DRAW_PROPOSAL://3
			buff[2] = 3;
			break;
		case DRAW_ACCEPT://4
			buff[2] = 4;
			break;
		case DRAW_REJECT://5
			buff[2] = 5;
			break;
		case FORFEIT://6
			buff[2] = 6;
			break;
		}
		free(playerMove.pSpecialMove);
		playerMove.pSpecialMove = NULL;
	}
	send(server_socket, buff, sizeof(char) * 10, 0);
	memset(buff, 0, sizeof(char) * 10);
	recv(server_socket, buff, sizeof(char) * 10, 0);
	if (buff[0] == 0x0E) {
		printf("Error: Server error. Aborting connection.\n");
		close(server_socket);
		return 0;
	}
	if (buff[0] != 0x0C) {
		printf("Error: Incorrect response from server. Aborting connection.\n");
		close(server_socket);
		return 0;
	}
	return 1;
}

void DrawWithChoice(GameState* pGameState, Color player, us_int is_colored) {
	if (is_colored)
		DrawColoredGameField(pGameState, player);
	else
		DrawGameField(pGameState, player);
}

void PlayOnlineChess() {
	ChessPiece** chessPieces = StandartChessPreset();
	GameState* pGameState = GameState_init(chessPieces, 32);
	us_int is_colored = RequestYesOrNo("Would you like to use colored version?");

	int server_socket = ConnectToServer();
	Color player = ServerHandshake(server_socket);

	Color winner = NONE;
	us_int err = 0;

	while (1 == 1) {
		err = !WaitForServer(pGameState, server_socket, player);
		DrawWithChoice(pGameState, player, is_colored);
		if (err || IsFinished(pGameState, &winner)) break;

		if (pGameState->draw_proposed)
			printf("%s proposes draw\n", (player == BLACK) ? "White" : "Black");
		err = !SendMoveToServer(pGameState, server_socket);
		DrawWithChoice(pGameState, player, is_colored);

		if (err || IsFinished(pGameState, &winner)) break;
	}

	close(server_socket);

	if (err) {
		GameState_fini(&pGameState);
		return;
	}

	switch (winner){
	case WHITE: printf("%s is the winner!", "White"); break;
	case BLACK: printf("%s is the winner!", "Black"); break;
	case NONE:  printf("Match ended in %s!", "draw"); break;
	}

	ListIter* pListIter = GetMovesLog(pGameState);
	char* pCurrent = ListIter_getnext(pListIter);
	printf("\n\nLOG:\n");
	us_int counter = 1;
	while (pCurrent != NULL) {
		printf("%d. %s ", counter, pCurrent);
		pCurrent = ListIter_getnext(pListIter);
		if (pCurrent != NULL) {
			printf("%s\n", pCurrent);
			pCurrent = ListIter_getnext(pListIter);
		}
		++counter;
	}
	printf("\n");
	ListIter_fini(&pListIter);

	GameState_fini(&pGameState);
}
