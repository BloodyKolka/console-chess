#include "console_ui.h"

#include <console_utils/input_tools.h>

#include <stdio.h>
#include <string.h>

enum _GameMode {
	ONLINE,
	HOTSEAT
};
typedef enum _GameMode GameMode;

GameMode RequestGameMode(){
	printf("Please, choose game mode:\n");
	printf("[hotseat]: Hot-Seat (local)\n");
	printf("[online] : Online\n");
	while (1) {
		char* input = readline();
		if (strcmp(input, "hotseat") == 0)
			return HOTSEAT;
		else if (strcmp(input, "online") == 0)
			return ONLINE;
		else printf("\nPlease, try again: ");
	}
}

int main(){
	GameMode gm = RequestGameMode();
	switch (gm) {
	case ONLINE : PlayOnlineChess(); break;
	case HOTSEAT: PlayChess();       break;
	}
	return 0;
}
