#include <utils/list.h>

#include <stdlib.h>

List* List_init() {
	List* ans = (List*)malloc(sizeof(List));
	ans->head = NULL;
	ans->tail = NULL;
	ans->size = 0;
	return ans;
}

void  List_fini(List** ppList) {
	if (ppList == NULL)
		return;
	if ((*ppList) == NULL)
		return;

	Node* pCur = (*ppList)->head;
	while (pCur != NULL){
		Node* pBuf = pCur->pNext;
		free(pCur);
		pCur = pBuf;
	}
	free(*ppList);
	*ppList = NULL;
}

void List_append(List* pList, void* new_el) {
	if (pList == NULL)
		return;
	if (new_el == NULL)
		return;

	Node* pNewNode = (Node*)malloc(sizeof(Node));
	pNewNode->pNext = NULL;
	pNewNode->pData = new_el;
	if (pList->size == 0){
		pNewNode->pPrev = NULL;
		pList->head = pNewNode;
	} else {
		pNewNode->pPrev = pList->tail;
		pList->tail->pNext = pNewNode;
	}
	++pList->size;
	pList->tail = pNewNode;
}

void List_remove(List* pList, void* del_el) {
	if (pList == NULL)
		return;
	if (del_el == NULL)
		return;
	if (pList->size == 0)
		return;

	if (pList->head->pData == del_el){
		Node* pBuf = pList->head->pNext;
		if (pList->head == pList->tail)
			pList->tail = NULL;
		free(pList->head);
		pList->head = pBuf;
		if (pBuf != NULL)
			pBuf->pPrev = NULL;
		--pList->size;
	} else {
		Node* pCur = pList->head;
		while ((pCur->pData != del_el) && (pCur->pNext != NULL))
			pCur = pCur->pNext;
		if (pCur->pData == del_el){
			pCur->pPrev->pNext = pCur->pNext;
			if (pCur->pNext != NULL)
				pCur->pNext->pPrev = pCur->pPrev;
			else
				pList->tail = pCur->pPrev;
			free(pCur);
			--pList->size;
		}
	}
}

ListIter* ListIter_init(List* pList) {
	if (pList == NULL)
		return NULL;
	ListIter* pAns = (ListIter*)malloc(sizeof(ListIter));
	pAns->pList = pList;
	pAns->pPrev = NULL;
	return pAns;
}

void ListIter_fini(ListIter** ppListIter) {
	if (ppListIter == NULL)
		return;
	if ((*ppListIter) == NULL)
		return;
	free(*ppListIter);
	*ppListIter = NULL;
}

void* ListIter_getnext(ListIter* pListIter){
	if (pListIter == NULL)
		return NULL;
	if (pListIter->pList == NULL)
		return NULL;

	if (pListIter->pPrev == NULL)
		pListIter->pPrev = pListIter->pList->head;
	else
		pListIter->pPrev = pListIter->pPrev->pNext;
	if (pListIter->pPrev == NULL)
		return NULL;
	else
		return pListIter->pPrev->pData;
}
