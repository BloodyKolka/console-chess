#ifndef UTILS__LIST_H
#define UTILS__LIST_H

#include <stddef.h>

struct _Node;
typedef struct _Node Node;
struct _Node {
	Node* pNext;
	Node* pPrev;
	void* pData;
};

struct _List {
	Node* head;
	Node* tail;
	size_t size;
};
typedef struct _List List;

List* List_init();
void  List_fini(List**);
void  List_append(List*, void* new_el);
void  List_remove(List*, void* del_el);

struct _ListIter{
	List* pList;
	Node* pPrev;
};
typedef struct _ListIter ListIter;

ListIter* ListIter_init(List*);
void 	  ListIter_fini(ListIter**);
void*     ListIter_getnext(ListIter*);

#endif /* UTILS__LIST_H */
