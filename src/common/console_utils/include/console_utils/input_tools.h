#ifndef CONSOLE_UTILS__INPUT_TOOLS_H
#define CONSOLE_UTILS__INPUT_TOOLS_H

char* readline();
int   readint(int min, int max);

#endif /* CONSOLE_UTILS__INPUT_TOOLS_H */
