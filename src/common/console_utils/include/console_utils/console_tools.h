#ifndef CONSOLE_UTILS__CONSOLE_TOOLS_H
#define CONSOLE_UTILS__CONSOLE_TOOLS_H

#include <stdio.h>

enum _ConsoleStyle{
    DEFAULT = 0,
    BRIGHT,
    DIM,
    ITALIC,
    UNDERLINED,
    BLINKING,
    RAPID_BLINKING,
    INVERSE,
    HIDDEN,
    CROSSED_OUT
};

typedef enum _ConsoleStyle ConsoleStyle;

void SetConsoleStyle(ConsoleStyle style);
void SetFontColor(int r, int g, int b);
void SetBackgroundColor(int r, int g, int b);
void ClearConsole();
void ResetConsoleColors();

#endif /* CONSOLE_UTILS__CONSOLE_TOOLS_H */
