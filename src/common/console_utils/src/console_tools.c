#include <console_utils/console_tools.h>

void SetConsoleStyle(ConsoleStyle style){
    char command[5];
    sprintf(command, "%c[%dm", 0x1B, style);
	printf("%s", command);
}

void SetFontColor(int r, int g, int b){
    if ((r < 0) || (r > 255))
        return;
    if ((g < 0) || (g > 255))
        return;
    if ((b < 0) || (b > 255))
        return;
    char command[18];
    sprintf(command, "%c[38;2;%d;%d;%dm", 0x1B, r, g, b);
	printf("%s", command);
}

void SetBackgroundColor(int r, int g, int b){
    if ((r < 0) || (r > 255))
        return;
    if ((g < 0) || (g > 255))
        return;
    if ((b < 0) || (b > 255))
        return;
    char command[18];
    sprintf(command, "%c[48;2;%d;%d;%dm", 0x1B, r, g, b);
	printf("%s", command);
}

void ResetConsoleColors(){
    char command[5];
    sprintf(command, "%c[0m", 0x1B);
    printf("%s", command);
}

void ClearConsole(){
    char command[7];
    sprintf(command, "%c[1;1H", 0x1B);
    printf("%s", command);
    sprintf(command, "%c[0J", 0x1B);
    printf("%s", command);
}
