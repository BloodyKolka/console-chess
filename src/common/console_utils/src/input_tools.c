#include <console_utils/input_tools.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char* readline(){
	char* pAns = (char*)malloc(sizeof(char) * 1);
	pAns[0] = '\0';
	char c = getchar();
	size_t cur_size = 0;
	while (c >= ' ') {
		char* pTmp = (char*)malloc(sizeof(char) * (cur_size + 2));
		strcpy(pTmp, pAns);
		free(pAns);
		pTmp[cur_size] = c;
		pTmp[cur_size + 1] = '\0';
		++cur_size;
		pAns = pTmp;
		c = getchar();
	}
	return pAns;
}
