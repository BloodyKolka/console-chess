#ifndef CHESS_LIB__CHESS_H
#define CHESS_LIB__CHESS_H

#include <utils/list.h>

enum _Color {
	WHITE = 0,
	BLACK = 1,
	NONE = 2
};
typedef enum _Color Color;

enum _Type {
	PAWN  ,
	ROOK  ,
	KNIGHT,
	BISHOP,
	QUEEN ,
	KING
};
typedef enum _Type Type;

enum _SpecialType {
	PROMOTION = 0,
	CASTLING_QUEENSIDE,
	CASTLING_KINGSIDE,
	DRAW_PROPOSAL,
	DRAW_ACCEPT,
	DRAW_REJECT,
	FORFEIT
};
typedef enum _SpecialType SpecialType;

struct _SpecialMove {
	Color player;
	Type promoteTo;
	SpecialType moveType;
};
typedef struct _SpecialMove SpecialMove;

enum PlayerStateMasks {
    M_KINGSIDECASTLING  = 0x1 ,//00000001
    M_QUEENSIDECASTLING = 0x2 ,//00000010
    M_CHECK 		    = 0x4 ,//00000100
    M_CHECKMATE 		= 0x8 ,//00001000
    M_ONE 				= 0xFF //11111111
};

typedef unsigned short us_int;

struct _ChessPiece;
typedef struct _ChessPiece ChessPiece;
struct _ChessPiece {
	ChessPiece** gamefield;
	us_int 		 column;
	us_int 		 row;
	Type   		 type;
	Color		 color;
};

struct _PlayerState {
	ChessPiece* enPassant;
	ChessPiece* pPromotion;
	us_int 		flags;
};
typedef struct _PlayerState PlayerState;

struct _GameState {
	List**	     chessPieces;
	List** 		 capturedPieces;
	ChessPiece** gamefield;
	ChessPiece** kings;
	PlayerState* playerStates;
	Color 		 currentPlayer;
	us_int       finished;
	us_int 		 draw_proposed;
	List*		 moveLog;
};
typedef struct _GameState GameState;

struct _MoveSupplementary {
	us_int* flagsChanges;
	ChessPiece* enPassantChange;
	ChessPiece* pCapured;
	ChessPiece* pPromotion;
};
typedef struct _MoveSupplementary MoveSup;

struct _Move {
	us_int   startColumn;
	us_int   startRow;
	us_int   finishColumn;
	us_int   finishRow;
	Color    player;
	MoveSup* pMoveSup;
};
typedef struct _Move Move;

struct _GeneralMove {
	Move* pMove;
	SpecialMove* pSpecialMove;
};
typedef struct _GeneralMove ChessMove;

ChessPiece** StandartChessPreset();

GameState* GameState_init(ChessPiece** chessPieces, size_t size);
void 	   GameState_fini(GameState** ppGameState);

us_int 	   MakeMove(GameState* pGameState, ChessMove* pMove);
us_int 	   IsFinished(GameState* pGameState, Color* pWinner);
ListIter*  GetCapturedPieces(GameState* pGameState, Color player);
ListIter*  GetMovesLog(GameState* pGameState);
Color 	   Inverse(Color);

#endif /* CHESS_LIB__CHESS_H */
