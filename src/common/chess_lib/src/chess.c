#include <chess_lib/chess.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

us_int us_abs(int x){
	if (x > 0)
		return x;
	else
		return -x;
}

int min(int a, int b){
	if (a <= b)
		return a;
	else
		return b;
}

int max(int a, int b){
	if (a >= b)
		return a;
	else
		return b;
}

Color Inverse(Color color){
	switch (color){
	case BLACK: return WHITE;
	case WHITE: return BLACK;
	case NONE : return NONE;//not invertible
	}
	return -1;//ERROR!
}

ChessPiece** StandartChessPreset(){
	ChessPiece** ans = (ChessPiece**)malloc(sizeof(ChessPiece*) * 32);
	ans[0] = (ChessPiece*)malloc(sizeof(ChessPiece));
	ans[0]->color     = WHITE;
	ans[0]->type      = ROOK;
	ans[0]->column    = 0;
	ans[0]->row       = 0;
	ans[0]->gamefield = NULL;

	ans[1] = (ChessPiece*)malloc(sizeof(ChessPiece));
	ans[1]->color     = WHITE;
	ans[1]->type      = KNIGHT;
	ans[1]->column    = 1;
	ans[1]->row       = 0;
	ans[1]->gamefield = NULL;

	ans[2] = (ChessPiece*)malloc(sizeof(ChessPiece));
	ans[2]->color     = WHITE;
	ans[2]->type      = BISHOP;
	ans[2]->column    = 2;
	ans[2]->row       = 0;
	ans[2]->gamefield = NULL;

	ans[3] = (ChessPiece*)malloc(sizeof(ChessPiece));
	ans[3]->color     = WHITE;
	ans[3]->type      = QUEEN;
	ans[3]->column    = 3;
	ans[3]->row       = 0;
	ans[3]->gamefield = NULL;

	ans[4] = (ChessPiece*)malloc(sizeof(ChessPiece));
	ans[4]->color     = WHITE;
	ans[4]->type      = KING;
	ans[4]->column    = 4;
	ans[4]->row       = 0;
	ans[4]->gamefield = NULL;

	ans[5] = (ChessPiece*)malloc(sizeof(ChessPiece));
	ans[5]->color     = WHITE;
	ans[5]->type      = BISHOP;
	ans[5]->column    = 5;
	ans[5]->row       = 0;
	ans[5]->gamefield = NULL;

	ans[6] = (ChessPiece*)malloc(sizeof(ChessPiece));
	ans[6]->color     = WHITE;
	ans[6]->type      = KNIGHT;
	ans[6]->column    = 6;
	ans[6]->row       = 0;
	ans[6]->gamefield = NULL;

	ans[7] = (ChessPiece*)malloc(sizeof(ChessPiece));
	ans[7]->color     = WHITE;
	ans[7]->type      = ROOK;
	ans[7]->column    = 7;
	ans[7]->row       = 0;
	ans[7]->gamefield = NULL;

	for (int i = 0; i < 8; ++i){
		ans[8 + i] = (ChessPiece*)malloc(sizeof(ChessPiece));
		ans[8 + i]->color     = WHITE;
		ans[8 + i]->type      = PAWN;
		ans[8 + i]->column    = i;
		ans[8 + i]->row       = 1;
		ans[8 + i]->gamefield = NULL;
	}

	for (int i = 0; i < 8; ++i){
		ans[16 + i] = (ChessPiece*)malloc(sizeof(ChessPiece));
		ans[16 + i]->color     = BLACK;
		ans[16 + i]->type      = PAWN;
		ans[16 + i]->column    = i;
		ans[16 + i]->row       = 6;
		ans[16 + i]->gamefield = NULL;
	}

	ans[24] = (ChessPiece*)malloc(sizeof(ChessPiece));
	ans[24]->color     = BLACK;
	ans[24]->type      = ROOK;
	ans[24]->column    = 0;
	ans[24]->row       = 7;
	ans[24]->gamefield = NULL;

	ans[25] = (ChessPiece*)malloc(sizeof(ChessPiece));
	ans[25]->color     = BLACK;
	ans[25]->type      = KNIGHT;
	ans[25]->column    = 1;
	ans[25]->row       = 7;
	ans[25]->gamefield = NULL;

	ans[26] = (ChessPiece*)malloc(sizeof(ChessPiece));
	ans[26]->color     = BLACK;
	ans[26]->type      = BISHOP;
	ans[26]->column    = 2;
	ans[26]->row       = 7;
	ans[26]->gamefield = NULL;

	ans[27] = (ChessPiece*)malloc(sizeof(ChessPiece));
	ans[27]->color     = BLACK;
	ans[27]->type      = QUEEN;
	ans[27]->column    = 3;
	ans[27]->row       = 7;
	ans[27]->gamefield = NULL;

	ans[28] = (ChessPiece*)malloc(sizeof(ChessPiece));
	ans[28]->color     = BLACK;
	ans[28]->type      = KING;
	ans[28]->column    = 4;
	ans[28]->row       = 7;
	ans[28]->gamefield = NULL;

	ans[29] = (ChessPiece*)malloc(sizeof(ChessPiece));
	ans[29]->color     = BLACK;
	ans[29]->type      = BISHOP;
	ans[29]->column    = 5;
	ans[29]->row       = 7;
	ans[29]->gamefield = NULL;

	ans[30] = (ChessPiece*)malloc(sizeof(ChessPiece));
	ans[30]->color     = BLACK;
	ans[30]->type      = KNIGHT;
	ans[30]->column    = 6;
	ans[30]->row       = 7;
	ans[30]->gamefield = NULL;

	ans[31] = (ChessPiece*)malloc(sizeof(ChessPiece));
	ans[31]->color     = BLACK;
	ans[31]->type      = ROOK;
	ans[31]->column    = 7;
	ans[31]->row       = 7;
	ans[31]->gamefield = NULL;

	return ans;
}

GameState* GameState_init(ChessPiece** chessPieces, size_t size) {
	if (chessPieces == NULL)
		return NULL;
	if (size == 0)
		return NULL;

	GameState* pAns = (GameState*)malloc(sizeof(GameState));

	pAns->gamefield = (ChessPiece**)calloc(8 * 8, sizeof(ChessPiece*));

	pAns->chessPieces = (List**)malloc(sizeof(List*) * 2);
	pAns->chessPieces[WHITE] = List_init();
	pAns->chessPieces[BLACK] = List_init();
	pAns->kings = (ChessPiece**)malloc(sizeof(ChessPiece*) * 2);
	for (size_t i = 0; i < size; ++i){
		if (chessPieces[i] == NULL)
			goto err;
		if ((chessPieces[i]->column < 0) || (chessPieces[i]->column > 7) ||
			(chessPieces[i]->row    < 0) || (chessPieces[i]->row    > 7))
			goto err;
		if (pAns->gamefield[chessPieces[i]->column + chessPieces[i]->row * 8] != NULL)
			goto err;

		pAns->gamefield[chessPieces[i]->column + chessPieces[i]->row * 8] = chessPieces[i];
		chessPieces[i]->gamefield = pAns->gamefield;
		if (chessPieces[i]->type == KING)
			pAns->kings[chessPieces[i]->color] = chessPieces[i];
		List_append(pAns->chessPieces[chessPieces[i]->color], chessPieces[i]);
		goto noerr;
		err:
			List_fini(&(pAns->chessPieces[WHITE]));
			List_fini(&(pAns->chessPieces[BLACK]));
			free(pAns->chessPieces);
			free(pAns->gamefield);
			free(pAns);
			return NULL;
		noerr:
			continue;
	}

	pAns->playerStates = (PlayerState*)malloc(sizeof(PlayerState) * 2);
	pAns->playerStates[WHITE].enPassant = NULL;
	pAns->playerStates[WHITE].flags = M_QUEENSIDECASTLING | M_KINGSIDECASTLING;
	pAns->playerStates[WHITE].pPromotion = NULL;
	pAns->playerStates[BLACK].enPassant = NULL;
	pAns->playerStates[BLACK].flags = M_QUEENSIDECASTLING | M_KINGSIDECASTLING;
	pAns->playerStates[BLACK].pPromotion = NULL;

	pAns->capturedPieces = (List**)malloc(sizeof(List*) * 2);
	pAns->capturedPieces[WHITE] = List_init();
	pAns->capturedPieces[BLACK] = List_init();

	pAns->currentPlayer = WHITE;
	pAns->finished = (0 == 1);
	pAns->draw_proposed = (0 == 1);
	pAns->moveLog = List_init();

	return pAns;
}

void GameState_fini(GameState** ppGameState) {
	if (ppGameState == NULL)
		return;
	if ((*ppGameState) == NULL)
		return;

	ListIter* pListIter = ListIter_init((*ppGameState)->chessPieces[WHITE]);
	ChessPiece* pCurrent = (ChessPiece*)ListIter_getnext(pListIter);
	while (pCurrent != NULL){
		free(pCurrent);
		pCurrent = (ChessPiece*)ListIter_getnext(pListIter);
	}
	ListIter_fini(&pListIter);

	pListIter = ListIter_init((*ppGameState)->chessPieces[BLACK]);
	pCurrent = (ChessPiece*)ListIter_getnext(pListIter);
	while (pCurrent != NULL){
		free(pCurrent);
		pCurrent = (ChessPiece*)ListIter_getnext(pListIter);
	}
	ListIter_fini(&pListIter);

	List_fini(&((*ppGameState)->chessPieces[WHITE]));
	List_fini(&((*ppGameState)->chessPieces[BLACK]));
	free((*ppGameState)->chessPieces);

	free((*ppGameState)->gamefield);

	free((*ppGameState)->playerStates);

	pListIter = ListIter_init((*ppGameState)->capturedPieces[BLACK]);
	pCurrent = (ChessPiece*)ListIter_getnext(pListIter);
	while (pCurrent != NULL){
		free(pCurrent);
		pCurrent = (ChessPiece*)ListIter_getnext(pListIter);
	}
	ListIter_fini(&pListIter);

	pListIter = ListIter_init((*ppGameState)->capturedPieces[BLACK]);
	pCurrent = (ChessPiece*)ListIter_getnext(pListIter);
	while (pCurrent != NULL){
		free(pCurrent);
		pCurrent = (ChessPiece*)ListIter_getnext(pListIter);
	}
	ListIter_fini(&pListIter);

	List_fini(&((*ppGameState)->capturedPieces[WHITE]));
	List_fini(&((*ppGameState)->capturedPieces[BLACK]));
	free((*ppGameState)->capturedPieces);

	pListIter = ListIter_init((*ppGameState)->moveLog);
	char* pCurMove = (char*)ListIter_getnext(pListIter);
	while (pCurMove != NULL){
		free(pCurMove);
		pCurMove = (char*)ListIter_getnext(pListIter);
	}
	ListIter_fini(&pListIter);
	List_fini(&((*ppGameState)->moveLog));

	free(*ppGameState);
	*ppGameState = NULL;
}

us_int ValidatePawnMove(GameState* pGameState, Move* pMove){
	us_int s_col = pMove->startColumn;
	us_int s_row = pMove->startRow;
	us_int f_col = pMove->finishColumn;
	us_int f_row = pMove->finishRow;
	if ((s_col < 0) || (s_col > 7) ||
		(s_row < 0) || (s_row > 7) ||
		(f_col < 0) || (f_col > 7) ||
		(f_row < 0) || (f_row > 7))
		return 0;
	if ((s_col == f_col) && (s_row == f_row))
		return 0;
	if (((f_col - s_col) >= 2) || ((s_col - f_col) >= 2))
		return 0;
	if (((f_row - s_row) >= 3) || ((s_row - f_row) >= 3))
		return 0;
	if (s_col == f_col) {
		if (pMove->player == WHITE){
			if (f_row <= s_row)
				return 0;
			if (f_row - s_row == 1)
				if (pGameState->gamefield[f_col + f_row * 8] == NULL) {
					if (f_row == 7)
						pMove->pMoveSup->pPromotion = pGameState->gamefield[s_col + s_row * 8];
					return 1;
				}
				else
					return 0;
			else
				if (s_row == 1){
					if (pGameState->gamefield[f_col + (f_row - 1) * 8] != NULL)
						return 0;
					pMove->pMoveSup->enPassantChange = pGameState->gamefield[s_col + s_row * 8];
					return 1;
				}
				else
					return 0;
		} else {
			if (f_row >= s_row)
				return 0;
			if (s_row - f_row == 1)
				if (pGameState->gamefield[f_col + f_row * 8] == NULL) {
					if (f_row == 0)
						pMove->pMoveSup->pPromotion = pGameState->gamefield[s_col + s_row * 8];
					return 1;
				}
				else
					return 0;
			else
				if (s_row == 6) {
					if (pGameState->gamefield[f_col + (f_row + 1) * 8] != NULL)
						return 0;
					pMove->pMoveSup->enPassantChange = pGameState->gamefield[s_col + s_row * 8];
					return 1;
				}
				else
					return 0;
		}
	} else {
		if (pMove->player == WHITE){
			if (f_row - s_row != 1)
				return 0;
			if ((s_col - f_col != 1) && (f_col - s_col != 1))
				return 0;
			if (pGameState->gamefield[f_col + f_row * 8] != NULL)
				if (pGameState->gamefield[f_col + f_row * 8]->color == pMove->player)
					return 0;
				else {
					pMove->pMoveSup->pCapured = pGameState->gamefield[f_col + f_row * 8];
					if (f_row == 7)
						pMove->pMoveSup->pPromotion = pGameState->gamefield[s_col + s_row * 8];
					return 1;
				}
			else {
				if (pGameState->playerStates[WHITE].enPassant == NULL)
					return 0;
				if (s_row != 4)
					return 0;
				if (f_row - 1 < 0)
					return 0;
				if (pGameState->gamefield[f_col + (f_row - 1) * 8] == NULL)
					return 0;
				if (pGameState->gamefield[f_col + (f_row - 1) * 8] == pGameState->playerStates[WHITE].enPassant) {
					pMove->pMoveSup->pCapured = pGameState->playerStates[WHITE].enPassant;
					return 1;
				}
				return 0;
			}
		} else {
			if (s_row - f_row != 1)
				return 0;
			if ((s_col - f_col != 1) && (f_col - s_col != 1))
				return 0;
			if (pGameState->gamefield[f_col + f_row * 8] != NULL)
				if (pGameState->gamefield[f_col + f_row * 8]->color == pMove->player)
					return 0;
				else {
					pMove->pMoveSup->pCapured = pGameState->gamefield[f_col + f_row * 8];
					if (f_row == 0)
						pMove->pMoveSup->pPromotion = pGameState->gamefield[s_col + s_row * 8];
					return 1;
				}
			else {
				if (pGameState->playerStates[BLACK].enPassant == NULL)
					return 0;
				if (s_row != 3)
					return 0;
				if (f_row + 1 > 7)
					return 0;
				if (pGameState->gamefield[f_col + (f_row + 1) * 8] == NULL)
					return 0;
				if (pGameState->gamefield[f_col + (f_row + 1) * 8] == pGameState->playerStates[BLACK].enPassant){
					pMove->pMoveSup->pCapured = pGameState->playerStates[BLACK].enPassant;
					return 1;
				}
				return 0;
			}
		}
	}
}

us_int ValidateRookMove(GameState* pGameState, Move* pMove){
	us_int s_col = pMove->startColumn;
	us_int s_row = pMove->startRow;
	us_int f_col = pMove->finishColumn;
	us_int f_row = pMove->finishRow;
	if ((s_col < 0) || (s_col > 7) ||
		(s_row < 0) || (s_row > 7) ||
		(f_col < 0) || (f_col > 7) ||
		(f_row < 0) || (f_row > 7))
		return 0;
	if ((s_col == f_col) && (s_row == f_row))
		return 0;
	if ((s_col != f_col) && (s_row != f_row))
		return 0;

	if (s_col == f_col){
		if (s_row < f_row) {
			for (us_int i = s_row + 1; i < f_row; ++i)
				if (pGameState->gamefield[s_col + i * 8] != NULL)
					return 0;
		} else {
			for (us_int i = s_row - 1; i > f_row; --i)
				if (pGameState->gamefield[s_col + i * 8] != NULL)
					return 0;
		}
	} else {
		if (s_col < f_col) {
			for (us_int i = s_col + 1; i < f_col; ++i)
				if (pGameState->gamefield[i + s_row * 8] != NULL)
					return 0;
		} else {
			for (us_int i = s_col - 1; i > f_col; --i)
				if (pGameState->gamefield[i + s_row * 8] != NULL)
					return 0;
		}
	}
	if (pGameState->gamefield[f_col + f_row * 8] != NULL) {
		if (pGameState->gamefield[f_col + f_row * 8]->color == pMove->player)
			return 0;
		else
			pMove->pMoveSup->pCapured = pGameState->gamefield[f_col + f_row * 8];
	}

	switch (pMove->player){
	case BLACK:
		if (s_row == 7){
			if (s_col == 0)
				pMove->pMoveSup->flagsChanges[pMove->player] &= ~M_KINGSIDECASTLING;
			else if (s_col == 7)
				pMove->pMoveSup->flagsChanges[pMove->player] &= ~M_QUEENSIDECASTLING;
		}
		break;
	case WHITE:
		if (s_row == 0){
			if (s_col == 0)
				pMove->pMoveSup->flagsChanges[pMove->player] &= ~M_KINGSIDECASTLING;
			else if (s_col == 7)
				pMove->pMoveSup->flagsChanges[pMove->player] &= ~M_QUEENSIDECASTLING;
		}
		break;
	case NONE:
		return 0;
		break;
	}
	return 1;
}

us_int ValidateKnightMove(GameState* pGameState, Move* pMove){
	us_int s_col = pMove->startColumn;
	us_int s_row = pMove->startRow;
	us_int f_col = pMove->finishColumn;
	us_int f_row = pMove->finishRow;
	if ((s_col < 0) || (s_col > 7) ||
		(s_row < 0) || (s_row > 7) ||
		(f_col < 0) || (f_col > 7) ||
		(f_row < 0) || (f_row > 7))
		return 0;
	us_int d_col = us_abs((int)s_col - (int)f_col);
	us_int d_row = us_abs((int)s_row - (int)f_row);
	us_int rules = 0;
	if (d_col == 2)
		if (d_row == 1)
			rules = 1;
	if (d_row == 2)
		if (d_col == 1)
			rules = 1;
	if (!rules)
		return 0;
	if (pGameState->gamefield[f_col + f_row * 8] != NULL) {
		if (pGameState->gamefield[f_col + f_row * 8]->color == pMove->player)
			return 0;
		else
			pMove->pMoveSup->pCapured = pGameState->gamefield[f_col + f_row * 8];
	}
	return 1;
}

us_int ValidateBishopMove(GameState* pGameState, Move* pMove){
	us_int s_col = pMove->startColumn;
	us_int s_row = pMove->startRow;
	us_int f_col = pMove->finishColumn;
	us_int f_row = pMove->finishRow;
	if ((s_col < 0) || (s_col > 7) ||
		(s_row < 0) || (s_row > 7) ||
		(f_col < 0) || (f_col > 7) ||
		(f_row < 0) || (f_row > 7))
		return 0;
	us_int d_col = us_abs((int)s_col - (int)f_col);
	us_int d_row = us_abs((int)s_row - (int)f_row);
	if (d_col != d_row)
		return 0;
	if (d_col == 0)
		return 0;

	if (s_col < f_col){
		if (s_row < f_row) {
			for (us_int i = 1; i < d_col; ++i)
				if (pGameState->gamefield[(s_col + i) + (s_row + i) * 8] != NULL)
					return 0;
		} else {
			for (us_int i = 1; i < d_col; ++i)
				if (pGameState->gamefield[(s_col + i) + (s_row - i) * 8] != NULL)
					return 0;
		}
	} else {
		if (s_row < f_row) {
			for (us_int i = 1; i < d_col; ++i)
				if (pGameState->gamefield[(s_col - i) + (s_row + i) * 8] != NULL)
					return 0;
		} else {
			for (us_int i = 1; i < d_col; ++i)
				if (pGameState->gamefield[(s_col - i) + (s_row - i) * 8] != NULL)
					return 0;
		}
	}
	if (pGameState->gamefield[f_col + f_row * 8] != NULL) {
		if (pGameState->gamefield[f_col + f_row * 8]->color == pMove->player)
			return 0;
		else
			pMove->pMoveSup->pCapured = pGameState->gamefield[f_col + f_row * 8];
	}
	return 1;
}

us_int ValidateQueenMove(GameState* pGameState, Move* pMove){
	return ValidateBishopMove(pGameState, pMove) || ValidateRookMove(pGameState, pMove);
}

us_int ValidateKingMove(GameState* pGameState, Move* pMove){
	us_int s_col = pMove->startColumn;
	us_int s_row = pMove->startRow;
	us_int f_col = pMove->finishColumn;
	us_int f_row = pMove->finishRow;
	if ((s_col < 0) || (s_col > 7) ||
		(s_row < 0) || (s_row > 7) ||
		(f_col < 0) || (f_col > 7) ||
		(f_row < 0) || (f_row > 7))
		return 0;
	us_int d_col = us_abs((int)s_col - (int)f_col);
	us_int d_row = us_abs((int)s_row - (int)f_row);
	if ((d_col > 1) || (d_row > 1))
		return 0;
	if (pGameState->gamefield[f_col + f_row * 8] != NULL) {
		if (pGameState->gamefield[f_col + f_row * 8]->color == pMove->player)
			return 0;
		else
			pMove->pMoveSup->pCapured = pGameState->gamefield[f_col + f_row * 8];
	}
	pMove->pMoveSup->flagsChanges[pMove->player] &= ~M_KINGSIDECASTLING;
	pMove->pMoveSup->flagsChanges[pMove->player] &= ~M_QUEENSIDECASTLING;
	return 1;
}

us_int IsPlayerChecked(GameState* pGameState, Color player);

us_int IsPlayerCheckmated(GameState* pGameState, Color player);

void MovePieceR(GameState* pGameState, Move* pMove, ChessPiece** pCapture);

void ResetMoveR(GameState* pGameState, Move* pMove, ChessPiece* pCapture);

us_int ValidateMove(GameState* pGameState, Move* pMove){
	if (pGameState == NULL)
		return 0;
	if (pMove == NULL)
		return 0;

	if ((pMove->startColumn < 0)  || (pMove->startColumn > 7)  ||
		(pMove->startRow < 0)     || (pMove->startRow > 7)     ||
		(pMove->finishColumn < 0) || (pMove->finishColumn > 7) ||
		(pMove->finishRow < 0)    || (pMove->finishRow > 7))
		return 0;

	if (pGameState->gamefield[pMove->startColumn + pMove->startRow * 8] == NULL)
		return 0;

	Color player = pGameState->gamefield[pMove->startColumn + pMove->startRow * 8]->color;
	if (player != pMove->player)
		return 0;

	pMove->pMoveSup = (MoveSup*)malloc(sizeof(MoveSup));
	pMove->pMoveSup->flagsChanges = (us_int*)malloc(sizeof(us_int) * 2);
	pMove->pMoveSup->flagsChanges[WHITE] = pGameState->playerStates[WHITE].flags;
	pMove->pMoveSup->flagsChanges[BLACK] = pGameState->playerStates[BLACK].flags;
	pMove->pMoveSup->pCapured = NULL;
	pMove->pMoveSup->enPassantChange = NULL;
	pMove->pMoveSup->pPromotion = NULL;

	us_int ans = 1;
	switch (pGameState->gamefield[pMove->startColumn + pMove->startRow * 8]->type){
	case PAWN  : ans = ValidatePawnMove  (pGameState, pMove); break;
	case ROOK  : ans = ValidateRookMove  (pGameState, pMove); break;
	case KNIGHT: ans = ValidateKnightMove(pGameState, pMove); break;
	case BISHOP: ans = ValidateBishopMove(pGameState, pMove); break;
	case QUEEN : ans = ValidateQueenMove (pGameState, pMove); break;
	case KING  : ans = ValidateKingMove  (pGameState, pMove); break;
	}
	if (!ans) {
		free(pMove->pMoveSup->flagsChanges);
		free(pMove->pMoveSup);
		pMove->pMoveSup = NULL;
	} else {
		ChessPiece* pCapture = NULL;
		MovePieceR(pGameState, pMove, &pCapture);
		if (!IsPlayerChecked(pGameState, player)){
			pMove->pMoveSup->flagsChanges[player] &= ~M_CHECK;
			if (IsPlayerChecked(pGameState, Inverse(player))) {
				pMove->pMoveSup->flagsChanges[Inverse(player)] |= M_CHECK;
				if (IsPlayerCheckmated(pGameState, Inverse(player)))
					pMove->pMoveSup->flagsChanges[Inverse(player)] |= M_CHECKMATE;
			}
		} else {
			free(pMove->pMoveSup->flagsChanges);
			free(pMove->pMoveSup);
			pMove->pMoveSup = NULL;
			ans = 0;
		}
		ResetMoveR(pGameState, pMove, pCapture);
	}
	return ans;
}

//Can be either checked with if or return amount of checking pieces
us_int IsCellChecked(GameState* pGameState, us_int column, us_int row, Color player, List* pList){
	ListIter* pListIter = ListIter_init(pGameState->chessPieces[Inverse(player)]);
	ChessPiece* pCurrent = (ChessPiece*)ListIter_getnext(pListIter);
	us_int ans = 0;
	Move test = {
		.startColumn = 0,
		.startRow = 0,
		.finishColumn = column,
		.finishRow = row,
		.pMoveSup = NULL,
		.player = Inverse(player)
	};
	test.pMoveSup = (MoveSup*)malloc(sizeof(MoveSup));
	test.pMoveSup->flagsChanges = (us_int*)malloc(sizeof(us_int) * 2);
	test.pMoveSup->flagsChanges[WHITE] = pGameState->playerStates[WHITE].flags;
	test.pMoveSup->flagsChanges[BLACK] = pGameState->playerStates[BLACK].flags;
	test.pMoveSup->pCapured = NULL;
	test.pMoveSup->enPassantChange = NULL;
	test.pMoveSup->pPromotion = NULL;
	us_int dans = 0;
	while (pCurrent != NULL){
		test.startColumn = pCurrent->column;
		test.startRow = pCurrent->row;
		switch (pCurrent->type){
		case PAWN:
			dans = ValidatePawnMove(pGameState, &test);
			break;
		case ROOK:
			dans = ValidateRookMove(pGameState, &test);
			break;
		case KNIGHT:
			dans = ValidateKnightMove(pGameState, &test);
			break;
		case BISHOP:
			dans = ValidateBishopMove(pGameState, &test);
			break;
		case QUEEN:
			dans = ValidateQueenMove(pGameState, &test);
			break;
		case KING:
			dans = ValidateKingMove(pGameState, &test);
			break;
		default  : dans = 0; break;//Should never happen
		}
		ans += dans;
		if ((dans != 0) && (pList != NULL))
			List_append(pList, pCurrent);
		pCurrent = (ChessPiece*)ListIter_getnext(pListIter);
	}
	ListIter_fini(&pListIter);
	free(test.pMoveSup->flagsChanges);
	free(test.pMoveSup);
	return ans;
}

us_int IsPlayerChecked(GameState* pGameState, Color player){
	return IsCellChecked(pGameState, pGameState->kings[player]->column, pGameState->kings[player]->row, player, NULL);
}

void MovePieceR(GameState* pGameState, Move* pMove, ChessPiece** pCapture){
	if (pGameState == NULL)
		return;
	if (pMove == NULL)
		return;
	if (pCapture == NULL)
		return;

	ChessPiece* pStart  = pGameState->gamefield[pMove->startColumn  + pMove->startRow  * 8];
	ChessPiece* pFinish = pGameState->gamefield[pMove->finishColumn + pMove->finishRow * 8];
	if (pStart == NULL)
		return;

	if (pFinish != NULL){
		*pCapture = pFinish;
		List_remove(pGameState->chessPieces[pFinish->color], pFinish);
		pGameState->gamefield[pMove->finishColumn + pMove->finishRow * 8] = NULL;
	} else
		*pCapture = NULL;
	pGameState->gamefield[pMove->startColumn + pMove->startRow * 8] = NULL;
	pGameState->gamefield[pMove->finishColumn + pMove->finishRow * 8] = pStart;
	pStart->column = pMove->finishColumn;
	pStart->row = pMove->finishRow;
}

void ResetMoveR(GameState* pGameState, Move* pMove, ChessPiece* pCapture){
	if (pGameState == NULL)
		return;
	if (pMove == NULL)
		return;

	ChessPiece* pStart  = pGameState->gamefield[pMove->startColumn  + pMove->startRow  * 8];
	ChessPiece* pFinish = pGameState->gamefield[pMove->finishColumn + pMove->finishRow * 8];
	if (pFinish == NULL)
		return;
	if (pStart != NULL)
		return;

	pGameState->gamefield[pMove->finishColumn + pMove->finishRow * 8] = pCapture;
	if (pCapture != NULL)
		List_append(pGameState->chessPieces[pCapture->color], pCapture);
	pGameState->gamefield[pMove->startColumn + pMove->startRow * 8] = pFinish;
	pFinish->column = pMove->startColumn;
	pFinish->row = pMove->startRow;
}

struct _Cell {
	us_int col;
	us_int row;
};
typedef struct _Cell Cell;

us_int IsPlayerCheckmated(GameState* pGameState, Color player){
	//Player is checked if
	//1) All adjacent cells are checked or obstructed by pieces of the same color
	//2) No move can result in capturing of all checking pieces
	//3) No move can obstruct line of attack of all checking pieces (if they have one)
	us_int NotCheckMated = 0;
	int col = pGameState->kings[player]->column;
	int row = pGameState->kings[player]->row;

	//Try to move the king (easiest to check)
	for (int i = max(0, col - 1); i <= min(7, col + 1); ++i)
		for (int j = max(0, row - 1); j <= min(7, row + 1); ++j){
			if ((i == col) && (j == col))
				continue;
			if (pGameState->gamefield[i + j * 8] != NULL)
				if (pGameState->gamefield[i + j * 8]->color == player)
					continue;
			ChessPiece* pCapture = NULL;
			Move test = {
				.startColumn = col,
				.startRow = row,
				.finishColumn = i,
				.finishRow = j,
				.player = player,
				.pMoveSup = NULL
			};
			MovePieceR(pGameState, &test, &pCapture);
			NotCheckMated = NotCheckMated || !(IsPlayerChecked(pGameState, player));
			ResetMoveR(pGameState, &test, pCapture);
		}

	if (NotCheckMated)
		return 0;

	List* pList = List_init();
	us_int check_count = IsCellChecked(pGameState, col, row, player, pList);

	//Try to capture checking piece (if possible)
	if (check_count == 1) {
		ChessPiece* attacker = (ChessPiece*)pList->head->pData;
		ListIter* pListIter = ListIter_init(pGameState->chessPieces[player]);
		ChessPiece* pCurrent = ListIter_getnext(pListIter);
		Move test = {
			.startColumn = 0,
			.startRow = 0,
			.finishColumn = attacker->column,
			.finishRow = attacker->row,
			.player = player,
			.pMoveSup = NULL
		};
		while (pCurrent != NULL){
			test.startColumn = pCurrent->column;
			test.startRow    = pCurrent->row;
			us_int ans = 0;
			ans = ValidateMove(pGameState, &test);
			if (test.pMoveSup != NULL){
				free(test.pMoveSup->flagsChanges);
				free(test.pMoveSup);
			}
			if (ans != 0) {
				NotCheckMated = 1;
				break;
			}
			pCurrent = ListIter_getnext(pListIter);
		}
		ListIter_fini(&pListIter);
	}

	if (NotCheckMated){
		List_fini(&pList);
		return 0;
	}

	//Try to block path for checking piece (if possible)
	if (check_count == 1) {
		ChessPiece* attacker = (ChessPiece*)pList->head->pData;
		us_int cell_count = 0;
		Cell*  cells = NULL;
		us_int s_col = attacker->column;
		us_int s_row = attacker->row;
		us_int f_col = col;
		us_int f_row = row;
		switch (attacker->type){
		case ROOK: {
			rook: {
			if (s_col == f_col){
				us_int delta = abs((int)f_row - (int)s_row);
				cell_count = delta - 1;
				if (cell_count != 0)
					cells = (Cell*)malloc(sizeof(Cell) * cell_count);

				if (s_row < f_row) {
					for (us_int i = 1; i < delta; ++i) {
						cells[i - 1].col = s_col;
						cells[i - 1].row = s_row + i;
					}
				} else {
					for (us_int i = 1; i < delta; ++i) {
						cells[i - 1].col = s_col;
						cells[i - 1].row = s_row - i;
					}
				}
			} else {
				us_int delta = abs((int)f_col - (int)s_col);
				cell_count = delta - 1;
				if (cell_count != 0)
					cells = (Cell*)malloc(sizeof(Cell) * cell_count);

				if (s_col < f_col) {
					for (us_int i = 1; i < delta; ++i) {
						cells[i - 1].col = s_col + i;
						cells[i - 1].row = s_row;
					}
				} else {
					for (us_int i = 1; i < delta; ++i) {
						cells[i - 1].col = s_col - i;
						cells[i - 1].row = s_row;
					}
				}
			}
			}
			break;
		}
		case BISHOP: {
			bishop: {
			us_int delta = us_abs((int)s_col - (int)f_col);
			cell_count = delta - 1;
			if (cell_count != 0)
				cells = (Cell*)malloc(sizeof(Cell) * cell_count);
			if (s_col < f_col){
				if (s_row < f_row) {
					for (us_int i = 1; i < delta; ++i) {
						cells[i - 1].col = (s_col + i);
						cells[i - 1].row = (s_row + i);
					}
				} else {
					for (us_int i = 1; i < delta; ++i) {
						cells[i - 1].col = (s_col + i);
						cells[i - 1].row = (s_row - i);
					}
				}
			} else {
				if (s_row < f_row) {
					for (us_int i = 1; i < delta; ++i) {
						cells[i - 1].col = (s_col - i);
						cells[i - 1].row = (s_row + i);
					}
				} else {
					for (us_int i = 1; i < delta; ++i) {
						cells[i - 1].col = (s_col - i);
						cells[i - 1].row = (s_row - i);
					}
				}
			}
			}
			break;
		}
		case QUEEN:
			if ((attacker->column == col) || (attacker->row == row))
				goto rook;
			else
				goto bishop;
			break;
		default: break;
		}
		if (cell_count != 0){
			us_int ans = 0;
			ListIter* pListIter = ListIter_init(pGameState->chessPieces[player]);
			ChessPiece* pCurrent = ListIter_getnext(pListIter);
			while ((pCurrent != NULL) && (!ans)){
				for (us_int i = 0; (i < cell_count) && (!ans); ++i){
					Move test = {
						.startColumn = pCurrent->column,
						.startRow = pCurrent->row,
						.finishColumn = cells[i].col,
						.finishRow = cells[i].row,
						.player = player,
						.pMoveSup = NULL
					};
					ans = ValidateMove(pGameState, &test);
					if (test.pMoveSup != NULL) {
						free(test.pMoveSup->flagsChanges);
						free(test.pMoveSup);
					}
				}
				pCurrent = ListIter_getnext(pListIter);
			}
			if (ans != 0)
				NotCheckMated = 1;
			ListIter_fini(&pListIter);
			free(cells);
		}
	}

	List_fini(&pList);

	if (NotCheckMated)
		return 0;

	return 1;
}

char* CreateRegularMoveLogEntry(GameState* pGameState, Move* pMove){
	char* pCurMove = (char*)malloc(sizeof(char) * 12);
	size_t len = 0;
	switch (pGameState->gamefield[pMove->startColumn + pMove->startRow * 8]->type){
	case PAWN  : break;
	case ROOK  : pCurMove[0] = 'R'; ++len; break;
	case KNIGHT: pCurMove[0] = 'N'; ++len; break;
	case BISHOP: pCurMove[0] = 'B'; ++len; break;
	case QUEEN : pCurMove[0] = 'Q'; ++len; break;
	case KING  : pCurMove[0] = 'K'; ++len; break;
	}

	List* pList = List_init();
	ListIter* pListIter = ListIter_init(pGameState->chessPieces[pMove->player]);
	ChessPiece* pCurrent = ListIter_getnext(pListIter);
	while (pCurrent != NULL){
		if (pCurrent->type == pGameState->gamefield[pMove->startColumn + pMove->startRow * 8]->type)
			if (pCurrent != pGameState->gamefield[pMove->startColumn + pMove->startRow * 8])
				List_append(pList, pCurrent);
		pCurrent = ListIter_getnext(pListIter);
	}
	ListIter_fini(&pListIter);

	us_int found_dups = 0;
	if (pList->size != 0) {
		ListIter* pPosDups = ListIter_init(pList);
		pCurrent = ListIter_getnext(pPosDups);
		us_int file_match = 0;
		us_int rank_math  = 0;
		while (pCurrent != NULL) {
			Move test = {
				.startColumn = pCurrent->column,
				.startRow = pCurrent->row,
				.finishColumn = pMove->finishColumn,
				.finishRow = pMove->finishRow,
				.player = pMove->player,
				.pMoveSup = NULL
			};
			us_int ans = ValidateMove(pGameState, &test);
			found_dups |= ans;
			if (ans){
				if (pCurrent->column == pMove->startColumn)
					file_match = 1;
				if (pCurrent->row == pMove->startRow)
					rank_math = 1;
			}
			if (test.pMoveSup != NULL){
				free(test.pMoveSup->flagsChanges);
				free(test.pMoveSup);
			}
			pCurrent = ListIter_getnext(pPosDups);
		}
		if (found_dups) {
			if (!file_match)
				pCurMove[len++] = pMove->startColumn + 'a';
			else if (!rank_math)
				pCurMove[len++] = pMove->startRow + '1';
			else {
				pCurMove[len++] = pMove->startColumn + 'a';
				pCurMove[len++] = pMove->startRow + '1';
			}
		}
		ListIter_fini(&pPosDups);
	}
	List_fini(&pList);

	if (pMove->pMoveSup->pCapured != NULL) {
		if (pGameState->gamefield[pMove->startColumn + pMove->startRow * 8]->type == PAWN)
			if (!found_dups)
				pCurMove[len++] = pMove->startColumn + 'a';
		pCurMove[len++] = 'x';
	}

	pCurMove[len++] = pMove->finishColumn + 'a';
	pCurMove[len++] = pMove->finishRow    + '1';

	if (pMove->pMoveSup->pCapured != NULL) {
		if (pMove->pMoveSup->pCapured == pGameState->playerStates[pMove->player].enPassant){
			pCurMove[len++] = 'e';
			pCurMove[len++] = '.';
			pCurMove[len++] = 'p';
			pCurMove[len++] = '.';
		}
	}
	if ((pMove->pMoveSup->flagsChanges[Inverse(pMove->player)] & M_CHECK) != 0)
		pCurMove[len++] = '+';

	pCurMove[len] = '\0';
	if (len < 11) {
		char* pTmp = (char*)malloc(sizeof(char) * (len + 1));
		strcpy(pTmp, pCurMove);
		free(pCurMove);
		pCurMove = pTmp;
	}
	return pCurMove;
}

void ApplyMoveEffects(GameState* pGameState, Move* pMove){
	if (pGameState == NULL)
		return;
	if (pMove == NULL)
		return;
	if (pMove->pMoveSup == NULL)
		return;

	List_append(pGameState->moveLog, CreateRegularMoveLogEntry(pGameState, pMove));
	if ((pMove->pMoveSup->flagsChanges[Inverse(pMove->player)] & M_CHECKMATE) != 0) {
		char* pCurMove = (char*)malloc(sizeof(char) * 2);
		pCurMove[0] = '#';
		pCurMove[1] = '\0';
		List_append(pGameState->moveLog, pCurMove);
		pCurMove = (char*)malloc(sizeof(char) * 4);
		pCurMove[0] = (pGameState->currentPlayer == WHITE) ? '1' : '0';
		pCurMove[1] = '-';
		pCurMove[2] = (pGameState->currentPlayer == WHITE) ? '0' : '1';
		pCurMove[3] = '\0';
		List_append(pGameState->moveLog, pCurMove);
	}

	if (pMove->pMoveSup->pCapured != NULL) {
		List_append(pGameState->capturedPieces[pMove->player], pMove->pMoveSup->pCapured);
		List_remove(pGameState->chessPieces[Inverse(pMove->player)], pMove->pMoveSup->pCapured);
		pGameState->gamefield[pMove->pMoveSup->pCapured->column + pMove->pMoveSup->pCapured->row * 8] = NULL;
	}
	//If promotion is available, force it, by not switching players
	if (pMove->pMoveSup->pPromotion == NULL)
		pGameState->currentPlayer = Inverse(pGameState->currentPlayer);
	pGameState->playerStates[WHITE].flags = pMove->pMoveSup->flagsChanges[WHITE];
	pGameState->playerStates[BLACK].flags = pMove->pMoveSup->flagsChanges[BLACK];
	pGameState->playerStates[Inverse(pMove->player)].enPassant = pMove->pMoveSup->enPassantChange;
	pGameState->playerStates[pMove->player].pPromotion = pMove->pMoveSup->pPromotion;

	ChessPiece* pStart = pGameState->gamefield[pMove->startColumn + pMove->startRow * 8];
	pStart->column = pMove->finishColumn;
	pStart->row = pMove->finishRow;

	pGameState->gamefield[pMove->startColumn  + pMove->startRow  * 8] = NULL;
	pGameState->gamefield[pMove->finishColumn + pMove->finishRow * 8] = pStart;

	if ((pGameState->playerStates[WHITE].flags & M_CHECKMATE) != 0) {
		pGameState->currentPlayer = BLACK;
		pGameState->finished = (1 == 1);
	} else if ((pGameState->playerStates[BLACK].flags & M_CHECKMATE) != 0) {
		pGameState->currentPlayer = WHITE;
		pGameState->finished = (1 == 1);
	}
}

us_int MakeRegularMove(GameState* pGameState, Move* pMove) {
	if (pGameState == NULL)
		return 0;
	if (pMove == NULL)
		return 0;
	if (pMove->player != pGameState->currentPlayer)
		return 0;
	//If promotion available, block move
	if (pGameState->playerStates[pMove->player].pPromotion != NULL)
		return 0;
	us_int ans = ValidateMove(pGameState, pMove);
	ans = ans && (pGameState->currentPlayer == pMove->player);
	if (ans)
		ApplyMoveEffects(pGameState, pMove);
	if (pMove->pMoveSup != NULL){
		if (pMove->pMoveSup->flagsChanges != NULL)
			free(pMove->pMoveSup->flagsChanges);
		free(pMove->pMoveSup);
		pMove->pMoveSup = NULL;
	}
	return ans;
}

us_int MakeSpecialMove(GameState* pGameState, SpecialMove* pSpecialMove) {
	if (pSpecialMove == NULL)
		return 0;
	if (pGameState->currentPlayer != pSpecialMove->player)
		return 0;

	switch (pSpecialMove->moveType){
	case PROMOTION: {
		if (pGameState->draw_proposed)
			return 0;
		if (pGameState->playerStates[pSpecialMove->player].pPromotion == NULL)
			return 0;
		if ((pSpecialMove->promoteTo == PAWN) || (pSpecialMove->promoteTo == KING))
			return 0;

		char* pCurMove = (char*)malloc(sizeof(char) * 1);
		pCurMove[0] = '\0';
		List_append(pGameState->moveLog, pCurMove);
		pCurMove = (char*)malloc(sizeof(char) * 4);
		pCurMove[0] = pGameState->playerStates[pSpecialMove->player].pPromotion->column + 'a';
		pCurMove[1] = pGameState->playerStates[pSpecialMove->player].pPromotion->row + '1';
		switch (pSpecialMove->promoteTo){
		case PAWN  : break;
		case ROOK  : pCurMove[2] = 'R'; break;
		case KNIGHT: pCurMove[2] = 'N'; break;
		case BISHOP: pCurMove[2] = 'B'; break;
		case QUEEN : pCurMove[2] = 'Q'; break;
		case KING  : break;
		}
		pCurMove[3] = '\0';
		List_append(pGameState->moveLog, pCurMove);
		pGameState->playerStates[pSpecialMove->player].pPromotion->type = pSpecialMove->promoteTo;
		pGameState->playerStates[pSpecialMove->player].pPromotion = NULL;
		break;
	}
	case CASTLING_QUEENSIDE: {
			if (pGameState->draw_proposed)
				return 0;
			if (pGameState->playerStates[pSpecialMove->player].pPromotion != NULL)
				return 0;
			if ((pGameState->playerStates[pSpecialMove->player].flags & M_QUEENSIDECASTLING) == 0)
				return 0;
			if ((pGameState->playerStates[pSpecialMove->player].flags & M_CHECK) != 0)
				return 0;
			if (pGameState->gamefield[1 + ((pSpecialMove->player == WHITE) ? 0 : 7) * 8] != NULL)
				return 0;
			if (pGameState->gamefield[2 + ((pSpecialMove->player == WHITE) ? 0 : 7) * 8] != NULL)
				return 0;
			if (pGameState->gamefield[3 + ((pSpecialMove->player == WHITE) ? 0 : 7) * 8] != NULL)
				return 0;
			for (us_int i = 4; i >= 2; --i)
				if (IsCellChecked(pGameState, i, (pSpecialMove->player == WHITE) ? 0 : 7, pSpecialMove->player, NULL))
					return 0;
			char* pCurMove = (char*)malloc(sizeof(char) * 6);
			pCurMove[0] = '0';
			pCurMove[1] = '-';
			pCurMove[2] = '0';
			pCurMove[3] = '-';
			pCurMove[4] = '0';
			pCurMove[5] = '\0';
			List_append(pGameState->moveLog, pCurMove);
			ChessPiece* pRook = pGameState->gamefield[0 + ((pSpecialMove->player == WHITE) ? 0 : 7) * 8];
			ChessPiece* pKing = pGameState->gamefield[4 + ((pSpecialMove->player == WHITE) ? 0 : 7) * 8];
			pGameState->gamefield[0 + ((pSpecialMove->player == WHITE) ? 0 : 7) * 8] = NULL;
			pGameState->gamefield[4 + ((pSpecialMove->player == WHITE) ? 0 : 7) * 8] = NULL;
			pGameState->gamefield[2 + ((pSpecialMove->player == WHITE) ? 0 : 7) * 8] = pKing;
			pGameState->gamefield[3 + ((pSpecialMove->player == WHITE) ? 0 : 7) * 8] = pRook;
			pRook->column = 3;
			pKing->column = 2;
			pGameState->playerStates[pSpecialMove->player].flags &= ~M_QUEENSIDECASTLING;
			pGameState->playerStates[pSpecialMove->player].flags &= ~M_KINGSIDECASTLING;
			break;
		}
	case CASTLING_KINGSIDE: {
			if (pGameState->draw_proposed)
				return 0;
			if (pGameState->playerStates[pSpecialMove->player].pPromotion != NULL)
				return 0;
			if ((pGameState->playerStates[pSpecialMove->player].flags & M_KINGSIDECASTLING) == 0)
				return 0;
			if ((pGameState->playerStates[pSpecialMove->player].flags & M_CHECK) != 0)
				return 0;
			if (pGameState->gamefield[5 + ((pSpecialMove->player == WHITE) ? 0 : 7) * 8] != NULL)
				return 0;
			if (pGameState->gamefield[6 + ((pSpecialMove->player == WHITE) ? 0 : 7) * 8] != NULL)
				return 0;
			for (us_int i = 4; i <= 6; ++i)
				if (IsCellChecked(pGameState, i, (pSpecialMove->player == WHITE) ? 0 : 7, pSpecialMove->player, NULL))
					return 0;
			char* pCurMove = (char*)malloc(sizeof(char) * 4);
			pCurMove[0] = '0';
			pCurMove[1] = '-';
			pCurMove[2] = '0';
			pCurMove[3] = '\0';
			List_append(pGameState->moveLog, pCurMove);
			ChessPiece* pRook = pGameState->gamefield[7 + ((pSpecialMove->player == WHITE) ? 0 : 7) * 8];
			ChessPiece* pKing = pGameState->gamefield[4 + ((pSpecialMove->player == WHITE) ? 0 : 7) * 8];
			pGameState->gamefield[7 + ((pSpecialMove->player == WHITE) ? 0 : 7) * 8] = NULL;
			pGameState->gamefield[4 + ((pSpecialMove->player == WHITE) ? 0 : 7) * 8] = NULL;
			pGameState->gamefield[6 + ((pSpecialMove->player == WHITE) ? 0 : 7) * 8] = pKing;
			pGameState->gamefield[5 + ((pSpecialMove->player == WHITE) ? 0 : 7) * 8] = pRook;
			pRook->column = 5;
			pKing->column = 6;
			pGameState->playerStates[pSpecialMove->player].flags &= ~M_KINGSIDECASTLING;
			pGameState->playerStates[pSpecialMove->player].flags &= ~M_QUEENSIDECASTLING;
			break;
		}
	case DRAW_PROPOSAL: {
		if (pGameState->draw_proposed)
			return 0;
		char* pCurMove = (char*)malloc(sizeof(char) * 4);
		pCurMove[0] = '(';
		pCurMove[1] = '=';
		pCurMove[2] = ')';
		pCurMove[3] = '\0';
		List_append(pGameState->moveLog, pCurMove);
		pGameState->draw_proposed = 1;
		pGameState->currentPlayer = Inverse(pGameState->currentPlayer);
		return 1;
		break;
	}
	case DRAW_ACCEPT: {
		pGameState->finished = 1;
		pGameState->currentPlayer = NONE;
		pGameState->currentPlayer = Inverse(pGameState->currentPlayer);
		char* pCurMove = (char*)malloc(sizeof(char) * 8);
		pCurMove[0] = '1';
		pCurMove[1] = '/';
		pCurMove[2] = '2';
		pCurMove[3] = '-';
		pCurMove[4] = '1';
		pCurMove[5] = '/';
		pCurMove[6] = '2';
		pCurMove[7] = '\0';
		List_append(pGameState->moveLog, pCurMove);
		return 1;
		break;
	}
	case DRAW_REJECT: {
		pGameState->draw_proposed = 0;
		pGameState->currentPlayer = Inverse(pGameState->currentPlayer);
		char* pCurMove = (char*)malloc(sizeof(char) * 1);
		pCurMove[0] = '\0';
		List_append(pGameState->moveLog, pCurMove);
		return 1;
		break;
	}
	case FORFEIT: {
		char* pCurMove = (char*)malloc(sizeof(char) * 4);
		pCurMove[0] = (pGameState->currentPlayer == WHITE) ? '0' : '1';
		pCurMove[1] = '-';
		pCurMove[2] = (pGameState->currentPlayer == WHITE) ? '1' : '0';
		pCurMove[3] = '\0';
		List_append(pGameState->moveLog, pCurMove);
		pGameState->finished = 1;
		pGameState->currentPlayer = Inverse(pGameState->currentPlayer);
		return 1;
		break;
	}
	}
	pGameState->playerStates[pSpecialMove->player].enPassant  = NULL;
	pGameState->currentPlayer = Inverse(pGameState->currentPlayer);

	if (IsPlayerChecked(pGameState, Inverse(pSpecialMove->player))) {
		pGameState->playerStates[Inverse(pSpecialMove->player)].flags |= M_CHECK;
		if (IsPlayerCheckmated(pGameState, Inverse(pSpecialMove->player)))
			pGameState->playerStates[Inverse(pSpecialMove->player)].flags |= M_CHECKMATE;
	}

	if ((pGameState->playerStates[WHITE].flags & M_CHECKMATE) != 0) {
		pGameState->currentPlayer = BLACK;
		pGameState->finished = (1 == 1);
	} else if ((pGameState->playerStates[BLACK].flags & M_CHECKMATE) != 0) {
		pGameState->currentPlayer = WHITE;
		pGameState->finished = (1 == 1);
	}
	return 1;
}

us_int MakeMove(GameState* pGameState, ChessMove* pChessMove){
	if (pGameState == NULL)
		return 0;
	if (pChessMove == NULL)
		return 0;
	if ((pChessMove->pMove != NULL) && (pChessMove->pSpecialMove != NULL))
		return 0;
	if ((pChessMove->pMove == NULL) && (pChessMove->pSpecialMove == NULL))
		return 0;
	if (pGameState->finished)
		return 0;

	if (pChessMove->pMove != NULL) {
		if (pGameState->draw_proposed)
			return 0;
		return MakeRegularMove(pGameState, pChessMove->pMove);
	}
	else
		return MakeSpecialMove(pGameState, pChessMove->pSpecialMove);

}

us_int IsFinished(GameState* pGameState, Color* pWinner){
	if (pGameState == NULL)
		return -1;
	if (pWinner == NULL)
		return -1;

	if (pGameState->finished)
		*pWinner = pGameState->currentPlayer;
	return pGameState->finished;
}

ListIter* GetCapturedPieces(GameState* pGameState, Color player){
	ListIter* pAns = ListIter_init(pGameState->capturedPieces[player]);
	return pAns;
}

ListIter* GetMovesLog(GameState* pGameState) {
	ListIter* pAns = ListIter_init(pGameState->moveLog);
	return pAns;
}
