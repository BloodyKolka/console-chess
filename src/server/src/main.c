#include <chess_lib/chess.h>

#if defined(__linux__)
#include <sys/socket.h>
#include <netinet/ip.h>
#else
#error "Only Linux supported!"
#endif

#include <unistd.h>
#include <pthread.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct _GameData;
typedef struct _GameData GameData;

struct _PlayerData {
	GameData* pGameData;
	Color     color;
	int 	  socketFD;
	us_int    notified;
};
typedef struct _PlayerData PlayerData;

struct _GameData {
	GameState*  pGameState;
	PlayerData* players;
	ChessMove   lastMove;
	Move* beforePromotion;
};

pthread_mutex_t mlock;

#define FINISH {\
	pthread_mutex_lock(&mlock);\
	pPlayerData->pGameData->pGameState->finished = 1;\
	pthread_mutex_unlock(&mlock);\
	return NULL;\
}

void send_error(int socket_fd) {
	char buff[10];
	buff[0] = 0x0E;
	send(socket_fd, buff, sizeof(char) * 10, 0);
}

void send_ok(int socket_fd) {
	char buff[10];
	buff[0] = 0x0C;
	send(socket_fd, buff, sizeof(char) * 10, 0);
}

int check_send_move(int socket_fd, ChessMove* pChessMove, Color target) {
	if (socket_fd < 0)
		return -1;
	if (pChessMove == NULL)
		return -2;

	char buff[10];
	memset(buff, 0, sizeof(char) * 10);
	buff[0] = 0x0C;

	if (pChessMove->pMove != NULL){
		//If promotion became available player is not switched
		if (pChessMove->pMove->player != target) {
			buff[1] = 1;
			buff[2] = (char)((pChessMove->pMove->startColumn & 0xFF00) >> 8);
			buff[3] = (char)( pChessMove->pMove->startColumn & 0x00FF);
			buff[4] = (char)((pChessMove->pMove->startRow & 0xFF00) >> 8);
			buff[5] = (char)( pChessMove->pMove->startRow & 0x00FF);
			buff[6] = (char)((pChessMove->pMove->finishColumn & 0xFF00) >> 8);
			buff[7] = (char)( pChessMove->pMove->finishColumn & 0x00FF);
			buff[8] = (char)((pChessMove->pMove->finishRow & 0xFF00) >> 8);
			buff[9] = (char)( pChessMove->pMove->finishRow & 0x00FF);
			free(pChessMove->pMove);
			pChessMove->pMove = NULL;
		}
	} else if (pChessMove->pSpecialMove != NULL) {
		buff[1] = 2;
		switch (pChessMove->pSpecialMove->moveType) {
		case PROMOTION://0
			buff[2] = 0;
			buff[3] = (char)pChessMove->pSpecialMove->promoteTo;
			break;
		case CASTLING_QUEENSIDE://1
			buff[2] = 1;
			break;
		case CASTLING_KINGSIDE://2
			buff[2] = 2;
			break;
		case DRAW_PROPOSAL://3
			buff[2] = 3;
			break;
		case DRAW_ACCEPT://4
			buff[2] = 4;
			break;
		case DRAW_REJECT://5
			buff[2] = 5;
			break;
		case FORFEIT://6
			buff[2] = 6;
			break;
		default: {
			send_error(socket_fd);
			return -3;
		}
		}
		free(pChessMove->pSpecialMove);
		pChessMove->pSpecialMove = NULL;
	}
	send(socket_fd, buff, sizeof(char) * 10, 0);
	return 0;
}

int get_move(int socket_fd, ChessMove* pChessMove, Color source) {
	if (socket_fd < 0)
		return -1;
	if (pChessMove == NULL)
		return -2;

	char buff[10];
	buff[0] = 0x0C;
	recv(socket_fd, buff, sizeof(char) * 10, 0);
	if (buff[0] != 0x0C) {
		send_error(socket_fd);
		return -3;
	}
	if (pChessMove->pMove != NULL)
		free(pChessMove->pMove);
	if (pChessMove->pSpecialMove != NULL)
		free(pChessMove->pSpecialMove);
	switch (buff[1]){
	case 1:
		pChessMove->pMove = (Move*)malloc(sizeof(Move));
		pChessMove->pMove->startColumn  = ((us_int)buff[2] << 8) | (us_int)buff[3];
		pChessMove->pMove->startRow     = ((us_int)buff[4] << 8) | (us_int)buff[5];
		pChessMove->pMove->finishColumn = ((us_int)buff[6] << 8) | (us_int)buff[7];
		pChessMove->pMove->finishRow    = ((us_int)buff[8] << 8) | (us_int)buff[9];
		pChessMove->pMove->player 		 = source;
		pChessMove->pMove->pMoveSup 	 = NULL;
		break;
	case 2:
		pChessMove->pSpecialMove = (SpecialMove*)malloc(sizeof(SpecialMove));
		pChessMove->pSpecialMove->player = source;
		switch (buff[2]) {
		case 0://PROMOTION
			pChessMove->pSpecialMove->moveType = PROMOTION;
			pChessMove->pSpecialMove->promoteTo = (Type)buff[3];
			break;
		case 1://CASTLING_QUEENSIDE
			pChessMove->pSpecialMove->moveType = CASTLING_QUEENSIDE;
			break;
		case 2://CASTLING_KINGSIDE
			pChessMove->pSpecialMove->moveType = CASTLING_KINGSIDE;
			break;
		case 3://DRAW_PROPOSAL
			pChessMove->pSpecialMove->moveType = DRAW_PROPOSAL;
			break;
		case 4://DRAW_ACCEPT
			pChessMove->pSpecialMove->moveType = DRAW_ACCEPT;
			break;
		case 5://DRAW_REJECT
			pChessMove->pSpecialMove->moveType = DRAW_REJECT;
			break;
		case 6://FORFEIT
			pChessMove->pSpecialMove->moveType = FORFEIT;
			break;
		default: {
			send_error(socket_fd);
			return -4;
		}
		}
		break;
	default: {
		send_error(socket_fd);
		return -5;
	}
	}
	return 0;
}

void* communicate_loop(void *arg) {
	PlayerData* pPlayerData = (PlayerData*)arg;
	while (1 == 1) {
		pthread_mutex_lock(&mlock);
		us_int game_finished = pPlayerData->pGameData->pGameState->finished;
		Color current_player = pPlayerData->pGameData->pGameState->currentPlayer;
		pthread_mutex_unlock(&mlock);
		if (game_finished) {
			if (!pPlayerData->notified)
				check_send_move(pPlayerData->socketFD, &pPlayerData->pGameData->lastMove, pPlayerData->color);
			return NULL;
		} else if (current_player == pPlayerData->color) {
			ChessMove move = {
				.pMove = NULL,
				.pSpecialMove = NULL
			};
			if (pPlayerData->pGameData->beforePromotion != NULL) {
				move.pMove = pPlayerData->pGameData->beforePromotion;
				if (check_send_move(pPlayerData->socketFD, &move, pPlayerData->color) != 0)
					FINISH
				pPlayerData->pGameData->beforePromotion = NULL;
			}
			if (check_send_move(pPlayerData->socketFD, &pPlayerData->pGameData->lastMove, pPlayerData->color) != 0)
				FINISH
			if (get_move(pPlayerData->socketFD, &move, pPlayerData->color) != 0)
				FINISH
			pthread_mutex_lock(&mlock);
			us_int res = MakeMove(pPlayerData->pGameData->pGameState, &move);
			if (res) {
				send_ok(pPlayerData->socketFD);
				if (move.pSpecialMove != NULL)
					if (move.pSpecialMove->moveType == PROMOTION)
						pPlayerData->pGameData->beforePromotion = pPlayerData->pGameData->lastMove.pMove;
				pPlayerData->pGameData->lastMove = move;
			} else {
				printf("INCORRECT MOVE!");
				send_error(pPlayerData->socketFD);
				pPlayerData->pGameData->pGameState->finished = 1;
				pthread_mutex_unlock(&mlock);
				return NULL;
			}
			pthread_mutex_unlock(&mlock);
		}
		sleep(1);
	}
	return NULL;
}

int main(){
	int listener_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (listener_socket_fd == -1) {
		printf("Error: Socket could not be created\n");
		goto finish;
	}
	//int optval = 1;
	//printf("%d", setsockopt(listener_socket_fd, SOL_SOCKET, SO_REUSEPORT, &optval, sizeof(int)));

	struct sockaddr_in server_addr;
	memset(&server_addr, 0, sizeof(struct sockaddr_in));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	server_addr.sin_port = htons(11374);

	GameData* pGameData = NULL;

	if ((bind(listener_socket_fd, (struct sockaddr*)&server_addr, sizeof(server_addr))) != 0) {
		printf("Error: Socket failed to bind\n");
		goto finish;
	}

	pGameData = (GameData*)malloc(sizeof(GameData));
	pGameData->pGameState = NULL;
	pGameData->lastMove.pMove = NULL;
	pGameData->lastMove.pSpecialMove = NULL;
	pGameData->beforePromotion = NULL;
	pGameData->players = (PlayerData*)malloc(sizeof(PlayerData) * 2);
	pGameData->players[0].pGameData = pGameData;
	pGameData->players[1].pGameData = pGameData;
	pGameData->players[0].color = WHITE;
	pGameData->players[1].color = BLACK;
	pGameData->players[1].notified = 0;
	pGameData->players[1].notified = 0;

	int connections = 0;
	if (listen(listener_socket_fd, 5) == -1) {
		printf("Error: Socket was unable to listen");
		goto finish;
	}
	while (connections < 2) {
		struct sockaddr_in client_address;
		unsigned int size = sizeof(client_address);
		pGameData->players[connections].socketFD = accept(listener_socket_fd, (struct sockaddr*)&client_address, &size);
		if (pGameData->players[connections].socketFD >= 0){
			char buff[2];
			buff[0] = 0x0A;
			buff[1] = (char)connections;
			send(pGameData->players[connections].socketFD, buff, sizeof(char) * 10, 0);
			memset(buff, 0, sizeof(char) * 10);
			recv(pGameData->players[connections].socketFD, buff, sizeof(char) * 10, 0);
			if (buff[0] != 0x0A)
				close(pGameData->players[connections].socketFD);
			else
				++connections;
		}
	}

	/*
	 * 1 byte - message type
	 * 8 byte - message data
	*/
	if (pthread_mutex_init(&mlock, NULL) == -1)
		goto finish;

	ChessPiece** chessPieces = StandartChessPreset();
	pGameData->pGameState = GameState_init(chessPieces, 32);
	if (pGameData == NULL)
		goto finish;

	pthread_t player1_td, player2_td;
	pthread_create(&player1_td, NULL, &communicate_loop, &pGameData->players[0]);
	pthread_create(&player2_td, NULL, &communicate_loop, &pGameData->players[1]);
	void* tmp;
	pthread_join(player1_td, &tmp);
	pthread_join(player2_td, &tmp);

	finish:
	if (pGameData != NULL) {
		if (pGameData->players[0].socketFD >= 0)
			close(pGameData->players[0].socketFD);
		if (pGameData->players[1].socketFD >= 0)
			close(pGameData->players[1].socketFD);
		free(pGameData->players);
		if (pGameData->pGameState != NULL)
			GameState_fini(&(pGameData->pGameState));
		free(pGameData);
	}
	if (listener_socket_fd >= 0)
		close(listener_socket_fd);
	pthread_mutex_destroy(&mlock);
	return 0;
}
